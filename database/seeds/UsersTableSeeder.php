<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

		$user = new User();
		$user->email = "user@user.lt";
		$user->password = bcrypt("secret");
		$user->name = "Vartotojas 1";
		$user->save();
    }
}
