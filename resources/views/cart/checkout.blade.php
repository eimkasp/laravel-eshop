@extends('layouts.shop')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>Checkout information </h1>
                <form action="{{ route('order.store') }}" method="post">
                    @csrf

                   <div class="form-group">
                       <label>Full name</label>
                       <input required type="text" name="full_name" class="form-control">
                   </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input required type="email" name="email" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Address </label>
                        <input required type="text" name="delivery_address" class="form-control">
                    </div>

                    <input type="submit" value="BUY" class="btn btn-primary btn-block">

                </form>
            </div>
        </div>
    </div>
@endsection