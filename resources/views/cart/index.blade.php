@extends('layouts.shop')


@section('content')
    <div class="cart-main-area ptb--100 bg__white">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form action="#">
                        <div class="table-content table-responsive">
                            <table>
                                <thead>
                                <tr>
                                    <th class="product-thumbnail">products</th>
                                    <th class="product-name">name of products</th>
                                    <th class="product-price">Price</th>
                                    <th class="product-quantity">Quantity</th>
                                    <th class="product-subtotal">Total</th>
                                    <th class="product-remove">Remove</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td class="product-thumbnail">
                                            <a href="{{ route('products.show', [$product->id]) }}">
                                                <img src="{{ $product->image }}" alt="product img">
                                            </a>
                                        </td>
                                        <td class="product-name">
                                            <a href="{{ route('products.show', [$product->id]) }}">
                                                {{ $product->title }}
                                            </a>
                                            <ul class="pro__prize">
                                                <li>{{ $product->price }}€</li>
                                            </ul>
                                        </td>
                                        <td class="product-price">
                                            <span class="amount">
                                                {{ $product->price * $product->quantity }}€
                                            </span>
                                        </td>
                                        <td class="product-quantity">
                                            <input type="number" value="{{ $product->quantity }}">
                                        </td>
                                        <td class="product-subtotal">
                                            {{ $product->price * $product->quantity }}€
                                        </td>
                                        <td class="product-remove"><a href="#"><i class="icon-trash icons"></i></a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="buttons-cart--inner">
                                    <div class="buttons-cart">
                                        <a href="{{ route('shop') }}">Continue Shopping</a>
                                    </div>
                                    <div class="buttons-cart checkout--btn">
                                        <a href="#">update</a>
                                        <a href="{{ route('cart.checkout') }}">checkout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">

                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 smt-40 xmt-40">
                                <div class="htc__cart__total">
                                    <h6>cart total</h6>
                                    <div class="cart__desk__list">
                                        <ul class="cart__desc">
                                            <li>cart total</li>
                                            <li>shipping</li>
                                        </ul>
                                        <ul class="cart__price">
                                            <li>{{ $cartTotal }}€</li>
                                            <li>0</li>
                                        </ul>
                                    </div>
                                    <div class="cart__total">
                                        <span>order total</span>
                                        <span>{{ $cartTotal }}€</span>
                                    </div>
                                    <ul class="payment__btn">
                                        <li class="active"><a href="#">payment</a></li>
                                        <li><a href="{{ route('shop') }}">continue shopping</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection