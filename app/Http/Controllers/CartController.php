<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //


		$products = User::cartItems();
		$cartTotal = User::cartValue();


		return view('cart.index', [
			'products' => $products,
			'cartTotal' => $cartTotal
		]);
    }

    public function checkout() {

    	return view('cart.checkout');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


		$product = Product::findOrFail($request->input('product_id'));


		// gauname esamus krepselio duomenis is sessijos
		$cart = session()->get('cart');


		// patikriname ar krepselis jau yra sukurtas
		if($cart == null) {
			// jei vartotojas neturi sukurto krepselio, sukuriame ji kaip tuscia masyva
			$cart = [];
		}

		/* patikriname ar pasirinkta preke jau ideta i krepseli */
		if(array_key_exists($product->id, $cart)) {
			// jei preke egzistuoja, tai prie jos kiekio pridedame vieneta
			$cart[$product->id]['quantity']++;
		} else {
			// jei preke neegzistuoja tada sukuriame nauja masyvo elementa
			$cart[$product->id] = [
				'id' => $product->id,
				'title' => $product->title,
				'image' => $product->image,
				'price' => $product->price,
				'quantity' => 1
			];
		}


		session()->put('cart', $cart);

		return redirect()->route('cart.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
