<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

		/* Sukuriame uzsakymo objekta */
		$order = new Order();
		$order->email = $request->input('email');
		$order->full_name = $request->input('full_name');
		$order->delivery_address = $request->input('delivery_address');

		$order->status = 'pending_payment';
		$order->save();

		$cartItems = User::cartItems();

		/* uzsakymui priskiriame prekes kurios buvo krepselyje */
		foreach ($cartItems as $item) {
			$orderItem = new OrderItem();
			$orderItem->order_id = $order->id;
			$orderItem->product_id = $item->id;
			$orderItem->quantity = $item->quantity;
			$orderItem->save();
		}

		// isvalome krepseli
		session()->forget('cart');

		return redirect()->route('paysera-redirect', [$order->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
