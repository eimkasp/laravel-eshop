<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //

	protected $table = 'orders';

	public function orderItems() {
		return $this->hasMany('App\OrderItem', 'order_id', 'id');
	}

	public function getOrderTotal() {
		$amount = 0;

		foreach ($this->orderItems as $item) {

			// gauname uzsakymo prekes kaina
			$price = $item->product->price * $item->quantity;

			// pridedame prie bendros uzsakymo kainos
			$amount += $price;
		}



		return $amount;
	}
}
